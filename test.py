import pytest # noqa
from mapper import parse_log_line, read_input
from reducer import read_mapper_output, reduce_data


def test_parse_log_line():
    """
    Test function which parse string from log like nginx and return ip and bytes by ip
    :return:
    """
    true_string = '69.162.113.213 - - [06/Apr/2009:18:44:29 +0200] ' \
                  '"GET /w00tw00t.at.ISC.SANS.DFind:) HTTP/1.1" 400 335 "-" "-" "-"'
    print(true_string)
    ip, val = parse_log_line(true_string)
    assert ip == '69.162.113.213'
    assert val == '335'

    string_without_argument = '- - [06/Apr/2009:18:44:29 +0200] ' \
                              '"GET /w00tw00t.at.ISC.SANS.DFind:) HTTP/1.1" 400 335 "-" "-" "-"'
    ip, val = parse_log_line(string_without_argument)

    print(ip, val)
    assert ip is None
    assert val == 0

    string_wit_bad_byte = '69.162.113.213 - - [06/Apr/2009:18:44:29 +0200] ' \
                          '"GET /w00tw00t.at.ISC.SANS.DFind:) HTTP/1.1" 400 - "-" "-" "-"'

    ip, val = parse_log_line(string_wit_bad_byte)

    print(ip, val)
    assert ip == '69.162.113.213'
    assert val == 0

    string_empty = ''
    ip, val = parse_log_line(string_empty)

    print(ip, val)
    assert ip is None
    assert val == 0


def test_read_input():
    """
    Test function which read from file log like nginx
    :return:
    """
    true_string = '69.162.113.213 - - [06/Apr/2009:18:44:29 +0200] ' \
                  '"GET /w00tw00t.at.ISC.SANS.DFind:) HTTP/1.1" 400 335 "-" "-" "-"'
    lines_list = [true_string, true_string]

    result = list(read_input(lines_list))
    assert len(result) == 2

    true_string = '69.162.113.213 - - [06/Apr/2009:18:44:29 +0200] ' \
                  '"GET /w00tw00t.at.ISC.SANS.DFind:) HTTP/1.1" 400 335 "-" "-" "-"'
    lines_list = [true_string, '']

    result = list(read_input(lines_list))
    assert len(result) == 1

    lines_list = ['', '\n']

    result = list(read_input(lines_list))
    assert len(result) == 0


def test_read_mapper_output():
    """
    Test read data from file/stdout after map stage and formating data for processing
    :return:
    """
    file = [
        '137.135.241.142,338',
        '5.135.160.159,331',
        '137.135.241.142,339',
        '5.135.160.159,0'
    ]

    result = list(read_mapper_output(file))

    assert len(result) == 4
    for ip, val in result:
        assert ip
        assert val


def test_reduce_data():
    """
    Test reduce data -> get count and total bytes be ip, data processed with dict where key is ip
    :return:
    """
    file = [
        '137.135.241.142,338',
        '5.135.160.159,331',
        '137.135.241.142,339',
        '5.135.160.159,0'
    ]
    data = read_mapper_output(file)
    result = reduce_data(data)
    assert len(result.keys()) == 2

    file = [
        '137.135.241.142,338',
        '5.135.160.159,331',
        '69.162.113.213,22',
        '5.135.160.158,0'
    ]
    data = read_mapper_output(file)
    result = reduce_data(data)
    assert len(result.keys()) == 4
