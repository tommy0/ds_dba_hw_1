#!/usr/bin/env python
import sys
import re
from typing import Generator, Iterable


"""
Use regex with sections for parse lines from log like nginx
Needs 
    <remote_host> = ip
    <bytes> = total bytes per request
"""
line_format = re.compile(r'^(?P<remote_host>[0-9]{,3}\.[0-9]{,3}\.[0-9]{,3}\.[0-9]{,3}) (?P<ident>[^ ]{1,}) '
                         r'(?P<remote_user>[^ ]{1,}|\-) \[(?P<datetime>[0-9]{2}\/[A-Za-z]{3}\/[0-9]{1,4}:[0-9]'
                         r'{1,2}:[0-9]{1,2}:[0-9]{1,2} [+\-][0-9]{4})\] \"(?P<method>[A-Z ]+) (?P<uri>[^\"]*) '
                         r'(?P<protocol>[^\"]*)\" (?P<status>[0-9]{3}) (?P<bytes>[0-9]{1,}|\-) '
                         r'\"(?P<referer>[^\"]*|\-)\" \"(?P<user_agent>[^\"]+)\" \"(?P<forwarded_for>[^\"]*|\-)\"$')


def parse_log_line(line: str) -> (str, int):
    """
    Parse line from log like nginx and get ip of remote host and number of transfer bytes
    :param line: string from log
    :return: ip host and number bytes
    """
    data = re.match(line_format, line)
    if data:
        datadict = data.groupdict()
        ip = datadict["remote_host"]
        bytes_sent = datadict["bytes"]
        if not bytes_sent.isnumeric():
            bytes_sent = 0
        return ip, bytes_sent
    return None, 0


def read_input(file: Iterable) -> Generator:
    """
    Iteration from log lines and call function of parsing and return results values of parsing
    :param lines_list: list read lines from file or stdin
    :return: Generator of ip host and number bytes
    """
    for line in file:
        try:
            ip, val = parse_log_line(line)
            if ip:
                yield parse_log_line(line)
        except Exception:
            continue


def print_data(data: Generator):
    """
    Print result data with processing for reduce
    :param data: Generator with data to print into stdout
    :return:
    """
    for ip, val in data:
        print(f'{ip},{val}')


def main():
    """
    Mapper for find in log like nginx ip and total bytes by request
    :return:
    """
    # input comes from STDIN (standard input)
    data = read_input(sys.stdin)
    print_data(data)


if __name__ == "__main__":
    main()
