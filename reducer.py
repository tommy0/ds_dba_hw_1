#!/usr/bin/env python
import sys
from typing import Iterable, Generator


def read_mapper_output(file: Iterable) -> Generator:
    """
    Element of data "ip,bytes" split and return
    :param file: data from mapper in stdin
    :return: ip and bytes
    """
    for line in file:
        yield line.rstrip().split(',', 1)


def reduce_data(data: Generator) -> dict:
    """
    Count total bytes and count by ip
    :param data: formated data with type (ip, bytes)
    :return: map with ip and total bytes and count
    """
    result = {}
    for ip, val in data:
        if ip not in result:
            result[ip] = [0, 0]
        result[ip][0] += int(val)
        result[ip][1] += 1
    return result


def output_data(data: dict):
    """
    Print reduced data from stdout with format ip,average_bytes_per_request,total_bytes
    :param data: formated data with type (ip, bytes)
    :return:
    """
    for ip, val in data.items():
        print("{0},{1:.1f},{2}".format(ip, val[0]/val[1], val[0]))


def main():
    """
    Reducer for aggregate total and average bytes of any uniq ip
    :return:
    """
    # input comes from STDIN (standard input)
    data = read_mapper_output(sys.stdin)
    result = reduce_data(data)
    output_data(result)


if __name__ == "__main__":
    main()
