## Scripts

* start_hadoop_nodes.sh (start hadoop nodes and yarn);
* copy_to_hdfs.sh (copy input directory logs into hdfs);
* start.sh (start mar reduce work on input data and write into output dir hdfs).
* del_output_hdfs.sh (remove old output artifacts)
* start_test.sh (start unit test for mapper and reducer)
* hdfs dfs -cat daily/output/* (watch the result of work map-reduce)

## Problem
Hadoop mapper and reducer for parse logs loke nginix/apache and get info about total bytes and average bytes per uniq ip